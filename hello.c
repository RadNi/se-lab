#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int verify(int rand){
    if ( rand * 100 > 50 )
        return 0;
    return 1;
}


int main() {
    srand(time(NULL));
    int r = rand();
    printf("%d\n", verify(r));
    return 0;
}
